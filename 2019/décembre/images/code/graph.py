import matplotlib.pyplot as plt
import numpy as np
from random import random
from math import exp, log1p

t = np.linspace(0, 10000, 10000)

n = [500]

def ep(i):
    return exp(-i/200+log1p(250))+250

for i in range(1, 10000):
    pas = 0
    if i < 600:
        if random()< 0.5:
            pas -= 1
        else:
            pas += 1
        n.append(n[-1]+(ep(i)-ep(i-1))+pas)
        # n[-1]+(ep(i-1)-ep(i))+pas
    else:
        if random()< ((200-n[-1])/100 +1):
            pas += 1
        else:
            pas -= 1

        n.append(n[-1]+pas)




plt.plot(t, [250]*10000, "-r", t,n, "-b")
plt.axis([0, 10000, 0, 500])
plt.xlabel("t", horizontalalignment='right', x=1.0)
plt.ylabel("n(t)", verticalalignment='top', y=0.9)


fig = plt.gcf()
fig.set_size_inches(18.5, 5.5) # inches = pouces (2.54 cm)
fig.savefig("../graph.png", dpi=300) # dpi = pixel par pouce (2.54 cm)
